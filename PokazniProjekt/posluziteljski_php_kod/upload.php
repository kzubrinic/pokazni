<?php

header('Content-Type: text/plain; charset=utf-8');

try {
    // Undefined | Multiple Files | $_FILES Corruption Attack
    // If this request falls under any of them, treat it invalid.
    if (!isset($_FILES['file']['error']) || is_array($_FILES['file']['error'])) {
        throw new RuntimeException('Invalid parameters.');
    }
    // lokacija za upload datoteke
    $target_dir = "uploads/";
    // novo ime datoteke
    $target_file = $target_dir . "baza.sqlite3";
    // Check $_FILES['file']['error'] value.
    // provjera grešaka u prijenosu
    switch ($_FILES['file']['error']) {
        case UPLOAD_ERR_OK:
            break;
        case UPLOAD_ERR_NO_FILE:
            throw new RuntimeException('No file sent.');
        case UPLOAD_ERR_INI_SIZE:
        case UPLOAD_ERR_FORM_SIZE:
            throw new RuntimeException('Exceeded filesize limit.');
        default:
            throw new RuntimeException('Unknown errors.');
    }
    // Provjera veličine datoteke. 
    if ($_FILES['file']['size'] > 1000000) {
        throw new RuntimeException('Exceeded filesize limit.');
    }
    // premješta datoteke iz temp foldera u definirano odredište
    if (!move_uploaded_file($_FILES['file']['tmp_name'], $target_file)) {
        throw new RuntimeException('Failed to move uploaded file.');
    }
    echo 'File is uploaded successfully.';
    // kompresira datoteku
    $filename = $target_dir . "baza.zip";
    if (file_exists($filename)) 
    	unlink($filename);
    $zip = new ZipArchive();
    if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
       exit("cannot open <$filename>\n");
    }
    $zip->addFile($target_file, "baza.sqlite3");
    $zip->close();
    if (file_exists($target_file)) 
    	unlink($target_file);
} catch (RuntimeException $e) {
   echo $e->getMessage();
}

?>