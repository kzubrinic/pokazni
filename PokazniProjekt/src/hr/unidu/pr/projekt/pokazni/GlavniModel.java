package hr.unidu.pr.projekt.pokazni;

import java.io.FileNotFoundException;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import hr.unidu.pr.projekt.pokazni.alati.Zapis;
import hr.unidu.pr.projekt.pokazni.baza.Baza;
import hr.unidu.pr.projekt.pokazni.baza.SqliteBaza;

/**
 * Glavni model aplikacije
 */
public class GlavniModel {
	private Map<String,Zapis> zapisi;
	private List<String> drzave ;
	private Map<String, String> postavke;

	private Map<String,Zapis> getZapisi(){
		return zapisi;
	}
	
	/**
	 * Vraća zapis sa zadanom šifrom
	 * @param koji	šifra zapisa koji se dohvaća
	 * @return	pročitani zapis postavke
	 */
	public Zapis getZapis(String koji){
		return zapisi.get(koji);
	}
	
	/**
	 * Getter metoda
	 * @return	vraća listu svih država
	 */
	public List<String> getDrzave(){
		return drzave;
	}
	
	/**
	 * Getter metoda koja vraća mapu svih postavki aplikacije.
	 * Svaka postavka je String, a ključ postavke je također String.
	 * @return	vraća mapu svih postavki
	 */
	public Map<String, String> getPostavke(){
		return postavke;
	}
	
	/**
	 * Konstruktor modela.
	 */
	public GlavniModel(){
		zapisi  = new HashMap<String,Zapis>();
		drzave  = new ArrayList<String>();
		procitajZapise();
	}

	/**
	 * Metoda iz baze čita sve zapise postavki aplikacije i sve postavke.
	 * @throws Exception 
	 */
	public void procitajZapise() {
		Baza b = new SqliteBaza();
		try {
			zapisi = b.citajSve(drzave);
			Locale locale = new Locale("hr", "HR");
		Collections.sort(drzave, Collator.getInstance(locale));
		postavke = b.citajPostavke();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
