package hr.unidu.pr.projekt.pokazni.baza;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.unidu.pr.projekt.pokazni.alati.Zapis;
/**
 * Implementacija rada s sqlite3 relacijskom bazom podataka.
 */
public class SqliteBaza implements Baza {
	Connection conn;
	public SqliteBaza() {
		Connection conn = this.connect();
	}
	private Connection connect() {
        String url = "jdbc:sqlite:baza/baza.sqlite3";
        
        try {
            if (conn == null)
            	conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return conn;
	}

	/**
	 * Metoda vraća mapu svih zapisa pročitanih iz baze
	 * @param drzave	lista u koju će se spremiti pročitane države
	 * @return	mapa svih zapisa
	 * @throws SQLException
	 */
	@Override
	public Map<String,Zapis> citajSve(List<String> drzave) throws Exception {
		String sql = "SELECT drzave.id, naziv, puni_naziv, en_naziv, "
				+	"en_puni_naziv, gradovi.naziv_grada as glavni_grad, kratica, himna, "
				+	"zastava, slika, dodatni_podaci "
				+	"FROM drzave, gradovi where drzave.id = gradovi.drzave_id ORDER BY naziv";
		Map<String,Zapis> zapisi = new HashMap<String,Zapis>();
        try (Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            while (rs.next()) {
            	zapisi.put(rs.getString("naziv"),new Zapis(rs.getString("naziv"), 
            			rs.getString("puni_naziv"), rs.getString("en_naziv"), 
            			rs.getString("en_puni_naziv"), rs.getString("glavni_grad"), 
            			rs.getString("kratica"), rs.getString("himna"),
            			rs.getString("zastava"), rs.getString("slika"),
            			rs.getString("dodatni_podaci"), rs.getInt("id")));
            	drzave.add(rs.getString("naziv"));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw e;
        }
        return zapisi;
	}
	
	/**
	 * Metoda vraća mapu svih postavki pročitanih iz baze
	 * @return	mapa svih postavki
	 * @throws SQLException
	 */
	@Override
	public Map<String,String> citajPostavke() throws Exception{
		String sql = "SELECT id, url_himne, url_mape, url_detalji, "
				+ "url_baza_down, url_baza_up "
				+ "FROM postavke ORDER BY id";
		Map<String,String> postavke = new HashMap<String,String>();
        try (Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)){
            rs.next();
           	postavke.put("url_himne",rs.getString("url_himne"));
           	postavke.put("url_mape",rs.getString("url_mape")); 
           	postavke.put("url_detalji",rs.getString("url_detalji"));
           	postavke.put("url_baza_down",rs.getString("url_baza_down"));
           	postavke.put("url_baza_up",rs.getString("url_baza_up"));
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            postavke = null;
            throw e;
        }
        return postavke;
	}

	/**
	 * Metoda za spremanje postavki
	 * @param postavke	mapa svih postavki koje se spremaju u bazu
	 * @throws SQLException
	 */
	@Override
	public void spremiPostavke(Map<String, String> postavke) throws Exception{
		String sql = "UPDATE postavke SET url_himne = ? , "
                + "url_mape = ?, url_detalji = ?, "
				+ "url_baza_down = ?, url_baza_up = ? "
                + "WHERE id = 1";
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, postavke.get("url_himne"));
            pstmt.setString(2, postavke.get("url_mape"));
            pstmt.setString(3, postavke.get("url_detalji"));
            pstmt.setString(4, postavke.get("url_baza_down"));
            pstmt.setString(5, postavke.get("url_baza_up"));
            pstmt.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            throw e;
        }		
	}
	
	/**
	 * Metoda za unos nove države
	 * @param z	zapis države koja se sprema u bazu
	 * @return	vraća id novog zapisa
	 * @throws SQLException
	 */
	@Override
	public int insertDrzava(Zapis z) throws Exception{
		String sql = "INSERT INTO drzave ("
				+ "naziv, puni_naziv, en_naziv, en_puni_naziv, "
                + "kratica, himna, zastava, "
				+ "slika, dodatni_podaci) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
		String sql2 = "INSERT INTO gradovi ("
				+ "drzave_id, naziv_grada) "
				+ "VALUES (?, ?)";
		int id = -1;
        try (PreparedStatement pstmt = conn.prepareStatement(sql);
        		PreparedStatement pstmt2 = conn.prepareStatement(sql2)) {
        	conn.setAutoCommit(false);
            pstmt.setString(1, z.getNaziv());
            pstmt.setString(2, z.getPuniNaziv());
            pstmt.setString(3, z.getEnNaziv());
            pstmt.setString(4, z.getEnPuniNaziv());
            pstmt.setString(5, z.getKratica());
            pstmt.setString(6, z.getHimna());
            pstmt.setString(7, z.getZastava());
            pstmt.setString(8, z.getSlika());
            pstmt.setString(9, z.getDetalji());
             // INSERT DRŽAVE
            pstmt.executeUpdate();
            // dohvati ID umetnutog zapisa
            ResultSet rs = pstmt.getGeneratedKeys();
            if (rs.next()) {
               id = rs.getInt(1);
            }
            pstmt2.setInt(1, id);
            pstmt2.setString(2, z.getGlavniGrad());
            // INSERT GRADA
            pstmt2.executeUpdate();
            conn.commit();
            
        } catch (SQLException e) {
        	// TO DO: hvatati razne vrste grešaka pri radu s bazom:
        	//   prvenstveno duplicate value i sl.
            System.out.println(e.getMessage());
            try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				throw e1;
			}
            throw e;
        } finally {
        	try {
        		if (conn != null)
        			conn.setAutoCommit(true);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw e;
			}
        }
		return id;
	}
	
	/**
	 * Metoda za izmjenu države
	 * @param z	zapis države koja se mijenja u bazi
	 * @throws SQLException
	 */
	@Override
	public void updateDrzava(Zapis z) throws Exception{
		String sql = "UPDATE drzave SET naziv = ? , "
                + "puni_naziv = ?, en_naziv = ?, en_puni_naziv = ?,"
                + "kratica = ?, himna = ?,"
                + "zastava = ?, slika = ?, dodatni_podaci = ? WHERE id = ?";
		String sql2 = "UPDATE gradovi SET naziv_grada = ?  "
					+ "WHERE drzave_id = ?";
        try (PreparedStatement pstmt = conn.prepareStatement(sql);
        		PreparedStatement pstmt2 = conn.prepareStatement(sql2)) {
        	conn.setAutoCommit(false);
            pstmt.setString(1, z.getNaziv());
            pstmt.setString(2, z.getPuniNaziv());
            pstmt.setString(3, z.getEnNaziv());
            pstmt.setString(4, z.getEnPuniNaziv());
            pstmt.setString(5, z.getKratica());
            pstmt.setString(6, z.getHimna());
            pstmt.setString(7, z.getZastava());
            pstmt.setString(8, z.getSlika());
            pstmt.setString(9, z.getDetalji());
            pstmt.setInt(10, z.getId());
            pstmt.executeUpdate();
            
            pstmt2.setString(1, z.getGlavniGrad());
            pstmt2.setInt(2, z.getId());
            // INSERT GRADA
            pstmt2.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
        	// TO DO: hvatati razne vrste grešaka pri radu s bazom:
        	//   prvenstveno duplicate value i sl.
            System.out.println(e.getMessage());
            try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				throw e1;
			}
            throw e;
        } finally {
        	try {
        		if (conn != null)
        			conn.setAutoCommit(true);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw e;
			}
        }
	}
	
	/**
	 * Metoda za brisanje države
	 * @param id	id države koja se briše iz baze
	 * @throws SQLException
	 */
	@Override
	public void deleteDrzava(int id) throws Exception{
		String sql = "DELETE FROM drzave WHERE id = ?";
		String sql2 = "DELETE FROM gradovi WHERE drzave_id = ?";
        try (PreparedStatement pstmt = conn.prepareStatement(sql);
        		PreparedStatement pstmt2 = conn.prepareStatement(sql2)) {
        	conn.setAutoCommit(false);
            // set the corresponding param
            pstmt.setInt(1, id);
             // update 
            pstmt.executeUpdate();
            
            pstmt2.setInt(1, id);
            // DELETE GRADA
            pstmt2.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
        	// TO DO: hvatati razne vrste grešaka pri radu s bazom:
        	//   prvenstveno duplicate value i sl.
            System.out.println(e.getMessage());
            try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				throw e1;
			}
            throw e;
        } finally {
        	try {
        		if (conn != null)
        			conn.setAutoCommit(true);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw e;
			}
        }
	}
}
