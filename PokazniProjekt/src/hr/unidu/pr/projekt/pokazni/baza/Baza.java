package hr.unidu.pr.projekt.pokazni.baza;

import java.util.List;
import java.util.Map;

import hr.unidu.pr.projekt.pokazni.alati.Zapis;
/**
 * Sučelje koje trebaju implementirati sve klase koje čitaju ili spremaju
 * podatke iz i u bazu.
 */
public interface Baza {
	/**
	 * Metoda vraća mapu svih zapisa pročitanih iz baze
	 * @param drzave	lista u koju će se spremiti pročitane države
	 * @return	mapa svih zapisa
	 * @throws Exception
	 */
	public abstract Map<String,Zapis> citajSve(List<String> drzave) throws Exception;
	
	/**
	 * Metoda vraća mapu svih postavki pročitanih iz baze
	 * @return	mapa svih postavki
	 * @throws Exception
	 */
	public abstract Map<String,String> citajPostavke() throws Exception;
	
	/**
	 * Metoda za spremanje postavki
	 * @param postavke	mapa svih postavki koje se spremaju u bazu
	 * @throws Exception
	 */
	public abstract void spremiPostavke(Map<String,String> postavke) throws Exception;

	/**
	 * Metoda za unos nove države
	 * @param z	zapis države koja se sprema u bazu
	 * @return	vraća id novog zapisa
	 * @throws Exception
	 */
	public abstract int insertDrzava(Zapis z) throws Exception;
	
	/**
	 * Metoda za izmjenu države
	 * @param z	zapis države koja se mijenja u bazi
	 * @throws Exception
	 */
	public abstract void updateDrzava(Zapis z) throws Exception;
	
	/**
	 * Metoda za brisanje države
	 * @param id	id države koja se briše iz baze
	 * @throws Exception
	 */
	public abstract void deleteDrzava(int id) throws Exception;
}
