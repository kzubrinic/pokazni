package hr.unidu.pr.projekt.pokazni;

import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractListModel;

/**
 * Model koji se koristi za oblikovanje sadržaja država za prikaz u listi država na početnoj formi.
 *
 */
public class DrzaveListModel<T> extends AbstractListModel<String> {
	private static final long serialVersionUID = -7668301248500817583L;
	private List<String> popisDrzava;
	private List<String> fPopisDrzava;
	
	/**
	 * Konstruktor koji prima listu država i inicijalizira model primljenom listom.
	 * @param popis	lista država kojom se inicijalizira lista
	 */
	public DrzaveListModel(List<String> popis) {
		super();
        popisDrzava = popis;
        fPopisDrzava = new ArrayList<String>();
        for (String e : popisDrzava)
            fPopisDrzava.add(e);
    }

	/**
	 * Vraća element na sa zadane pozicije liste
	 * @param index	pozicija s koje se dohvaća element
	 */
	@Override
	public String getElementAt(int index) {
		if ((index < fPopisDrzava.size()) && (index >=0))
            return fPopisDrzava.get(index);
        else
            return null;
	}

	@Override
	/**
	 * Getter metoda
	 * @return	broj elementa u listi država
	 */
	public int getSize() {
		return fPopisDrzava.size();
	}
	
	/**
	 * Dodaje novi element u listu
	 * @param o	element koji se dodaje u listu
	 */
	public void addElement(String o) {
        popisDrzava.add(o);
        fPopisDrzava.add((String)o);
        fireContentsChanged (this, 0, getSize());
    }
	
	/**
	 * Briše element iz liste
	 * @param o	element koji se briše iz liste
	 */
	public void remove(String o) {
        popisDrzava.remove(o);
        fPopisDrzava.remove(o);
        fireContentsChanged (this, 0, getSize());
    }
	
	/**
	 * Mijenja sadržaj zadanog elementa liste
	 * @param red	red liste koji se ažurira
	 * @param o	novi sadržaj koji će se spremiti na zadani red liste
	 */
	public void updateElement(int red, String o) {
        popisDrzava.set(red, o);
        fPopisDrzava.set(red, o);
        fireContentsChanged (this, 0, getSize());
    }
	
	/**
	 * Filtrira sadržaj liste kod prikaza. U listi se prikazuju samo oni elementi
	 * čiji naziv počinje zadanim tekstom.
	 * @param t	tekst koji služi za filtriranje sadržaja liste
	 */
	public void filter(String t){
		fPopisDrzava.clear();
		for (String e : popisDrzava){
			if (e.toLowerCase().startsWith(t.toLowerCase()) || t==null){
				fPopisDrzava.add(e);
			}
		}
		fireContentsChanged (this, 0, getSize());
	}
	
}
