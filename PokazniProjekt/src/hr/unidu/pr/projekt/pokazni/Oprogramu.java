package hr.unidu.pr.projekt.pokazni;

import javax.swing.JOptionPane;

/**
 * Ekran pomoći aplikacije.
 */
public class Oprogramu {
	/**
	 * Metoda koja prikazuje podatke o programu. 
	 */
	public static void prikaziInfooProgramu() {
		JOptionPane.showMessageDialog(null,
				"Program za prikaz informacija o svjetskim državama.\n"
			+	"Program prikazuje osnovne informacije o državama\n"
			+	"(naziv, glavni grad, zastavu, mapu i himnu).\n\n"
			+   "Korisnici mogu dodavati nove države ili mijenjati\n"
			+   "podatke postojećih. Slike država i mape su spremljene\n"
			+	"lokalno na disku, a himne se dohvaćaju s web stranice.\n\n"
			+	"Autor: Krunoslav Žubrinić 2018.", "O programu",
			JOptionPane.INFORMATION_MESSAGE);
	}
}
