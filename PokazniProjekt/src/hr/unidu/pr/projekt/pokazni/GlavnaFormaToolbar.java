package hr.unidu.pr.projekt.pokazni;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToolBar;

public class GlavnaFormaToolbar extends JToolBar {
	private JButton tipkaToolbarAdminDrzave;
	private JButton tipkaToolbarAdminPostavke;
	private JButton tipkaToolbarPomoc;
	
	/**
	 * Getter metoda
	 * @return	vraća Swing komponentu tipke u toolbaru
	 */
	public JButton getTipkaToolbarAdminDrzave() {
		return tipkaToolbarAdminDrzave;
	}
	
	/**
	 * Getter metoda
	 * @return	vraća Swing komponentu tipke u toolbaru
	 */
	public JButton getTipkaToolbarAdminPostavke() {
		return tipkaToolbarAdminPostavke;
	}
	
	/**
	 * Getter metoda
	 * @return	vraća Swing komponentu tipke u toolbaru
	 */
	public JButton getTipkaToolbarPomoc() {
		return tipkaToolbarPomoc;
	}
	
	/**
	 * Konstruktor koji stvara i inicijalizira toolbar
	 */
	public GlavnaFormaToolbar() {
		napuniGumbe();
		setFloatable(false);
	}
	
	private void napuniGumbe() {
		tipkaToolbarAdminDrzave = new JButton(); 
		tipkaToolbarAdminDrzave.setIcon(new ImageIcon("ikone/adminDrzava.png"));
		tipkaToolbarAdminDrzave.setToolTipText("Administracija država");
	    add(tipkaToolbarAdminDrzave);

	    tipkaToolbarAdminPostavke = new JButton(); 
		tipkaToolbarAdminPostavke.setIcon(new ImageIcon("ikone/adminPostavke.png"));
		tipkaToolbarAdminPostavke.setToolTipText("Administracija postavki");
	    add(tipkaToolbarAdminPostavke);

	    tipkaToolbarPomoc = new JButton(); 
		tipkaToolbarPomoc.setIcon(new ImageIcon("ikone/pomoc.png"));
		tipkaToolbarPomoc.setToolTipText("O programu");
	    add(tipkaToolbarPomoc);

	}
}
