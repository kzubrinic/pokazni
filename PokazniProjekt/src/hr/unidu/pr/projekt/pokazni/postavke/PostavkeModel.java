package hr.unidu.pr.projekt.pokazni.postavke;

import java.util.Map;

import hr.unidu.pr.projekt.pokazni.baza.Baza;
import hr.unidu.pr.projekt.pokazni.baza.SqliteBaza;
/**
 * Klasa modela postavki aplikacije
 */
public class PostavkeModel {
	
	/**
	 * Metoda dohvaća postavke iz baze
	 * @return	mapa svih postavki aplikacije
	 * @throws Exception
	 */
	public Map<String,String> dohvatiPostavke() throws Exception {
		Baza b = new SqliteBaza();
		return b.citajPostavke();
	}
	
	/**
	 * Metoda sprema postavke aplikacije
	 * @param postavke	mapa postavki koje se spremaju
	 * @throws Exception
	 */
	public void spremiPostavke(Map<String,String> postavke) throws Exception {
			Baza b = new SqliteBaza();
			b.spremiPostavke(postavke);
	}
}
