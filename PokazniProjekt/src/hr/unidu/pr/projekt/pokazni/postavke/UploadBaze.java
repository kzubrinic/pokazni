package hr.unidu.pr.projekt.pokazni.postavke;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
/**
 * Klasa za upload baze
 */
public class UploadBaze extends SwingWorker<Boolean, Void>{
	private Map<String, String> postavke;
	/**
	 * Konstruktor klase
	 * @param postavke	postavke koje se uploudaju
	 */
	public UploadBaze(Map<String, String> postavke) {
		this.postavke = postavke;
	}

	private boolean upload() throws Exception, IOException {
		CloseableHttpClient client = HttpClients.createDefault();
		String postUrl = postavke.get("url_baza_up");
		HttpPost httpPost = new HttpPost(postUrl);
		// HttpPost httpPost = new HttpPost("http://meditor.com.hr/pokazni/upload.php");
		// HttpPost httpPost = new HttpPost("http://localhost/primjeri/upload.php");

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		builder.addBinaryBody("file", new File("baza/baza.sqlite3"), ContentType.APPLICATION_OCTET_STREAM, "file.ext");
		HttpEntity multipart = builder.build();

		httpPost.setEntity(multipart);

		CloseableHttpResponse response = client.execute(httpPost);
		// System.out.println(response.getStatusLine().getStatusCode());
		// System.out.println(response.getEntity().getContent().toString());
		// String responseString = EntityUtils.toString(response.getEntity(), "UTF-8");
		// System.out.println(responseString);
		client.close();
		if (response.getStatusLine().getStatusCode() == 200)
			return true;
		else
			return false;
	}

	/**
	 * Operacija koja se izvršava u posebnoj dretvi u pozadini
	 */
	@Override
	protected Boolean doInBackground() throws Exception {
		return upload();
	}
	
	/**
	 * Po završetku izvođenje operacije u posebnoj dretvi u pozadini
	 * @exception	InterruptedException
	 * @exception	ExecutionException
	 */
	protected void done() {
		try {
			get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
