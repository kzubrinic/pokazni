package hr.unidu.pr.projekt.pokazni.postavke;

import java.awt.BorderLayout;
import java.io.FileNotFoundException;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import net.miginfocom.swing.MigLayout;

/**
 * Forma postavki aplikacije
 */
public class PostavkeForma {
	private JDialog ekran;
	private JPanel contentPane;
	private JTextField urlMape, urlHimne, urlDetalji, urlBazaDown, urlBazaUp;
	private JButton btnSpremi, btnKraj;
	
	/**
	 * Getter metoda
	 * @return	vraća formu
	 */
	public JDialog getEkran(){
		return ekran;
	}
	
	/**
	 * Getter metoda
	 * @return	vraća glavni kontejner
	 */
	public JPanel getContentPane() {
		return contentPane;
	}

	/**
	 * Getter metoda
	 * @return	vraća Swing kompomentu koja sadrži URL mape države
	 */
	public JTextField getUrlMape() {
		return urlMape;
	}

	/**
	 * Getter metoda
	 * @return	vraća Swing kompomentu koja sadrži URL himne države
	 */
	public JTextField getUrlHimne() {
		return urlHimne;
	}

	/**
	 * Getter metoda
	 * @return	vraća Swing kompomentu koja sadrži detalje države
	 */
	public JTextField getUrlDetalji() {
		return urlDetalji;
	}

	/**
	 * Getter metoda
	 * @return	vraća Swing kompomentu koja sadrži URL za download baze
	 */
	public JTextField getUrlBazaDown() {
		return urlBazaDown;
	}

	/**
	 * Getter metoda
	 * @return	vraća Swing kompomentu koja sadrži URL za upload baze
	 */
	public JTextField getUrlBazaUp() {
		return urlBazaUp;
	}

	/**
	 * Getter metoda
	 * @return	vraća Swing kompomentu koja sadrži gumb za spremanje
	 */
	public JButton getBtnSpremi() {
		return btnSpremi;
	}
	
	/**
	 * Getter metoda
	 * @return	vraća Swing kompomentu koja sadrži gumb za kraj rada
	 */
	public JButton getBtnKraj() {
		return btnKraj;
	}

	/**
	 * Konstruktor. Stvara i incijalizira formu..
	 */
	public PostavkeForma() {
		ekran = new JDialog();
		ekran.setTitle("Postavke aplikacije");
		ekran.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout());
		ekran.setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new MigLayout("wrap 2", "[][][]", "[][][][]"));

		JLabel lblUrlMape = new JLabel("URL mape");
		panel.add(lblUrlMape, "cell 1 0");

		urlMape = new JTextField(50);
		panel.add(urlMape, "cell 2 0");

		JLabel lblUrlHimne = new JLabel("URL himne");
		panel.add(lblUrlHimne, "cell 1 1");

		urlHimne = new JTextField(50);
		panel.add(urlHimne, "cell 2 1");
		
		JLabel lblUrlDetalji = new JLabel("URL detalja");
		panel.add(lblUrlDetalji, "cell 1 2");

		urlDetalji = new JTextField(50);
		panel.add(urlDetalji, "cell 2 2");
		
		JLabel lblUrlBazaDown = new JLabel("URL za download baze");
		panel.add(lblUrlBazaDown, "cell 1 3");

		urlBazaDown = new JTextField(50);
		panel.add(urlBazaDown, "cell 2 3");

		JLabel lblUrlBazaUp = new JLabel("URL za upload baze");
		panel.add(lblUrlBazaUp, "cell 1 4");

		urlBazaUp = new JTextField(50);
		panel.add(urlBazaUp, "cell 2 4");
		
		btnSpremi = new JButton("Spremi");
		panel.add(btnSpremi, "flowx,cell 2 5");

		btnKraj = new JButton("Odustani");
		panel.add(btnKraj, "cell 2 5");
		ekran.pack();
		ekran.setLocation(180, 180);
	}
}
