package hr.unidu.pr.projekt.pokazni.postavke;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.swing.SwingWorker;
/**
 * Klasa za download baze
 */
public class DownloadBaze extends SwingWorker<Boolean, Void>{
	private Map<String, String> postavke;
	private boolean izvorna;

	/**
	 * Konstruktor klase
	 * @param postavke	mapa u koju će se spremiti downlodane postavke
	 * @param izvorna	true - ako downlodamo izvorne postavke, false ako downlodamo korisničke postavke
	 */
	public DownloadBaze(Map<String, String> postavke, boolean izvorna) {
		this.postavke = postavke;
		this.izvorna = izvorna;
	}

	private boolean download() throws Exception, IOException {
		String url, datoteka;
		if (izvorna) {
			datoteka = "izvorna_baza.zip";
			url = "http://meditor.com.hr/pokazni/uploads/" + datoteka;
		}
		else {
			datoteka = "baza.zip";
			if (postavke == null)
				url = "http://meditor.com.hr/pokazni/uploads/" + datoteka;
			else
				url = postavke.get("url_baza_down");
		}
		File file = new File("baza/baza.zip");
		file.deleteOnExit();
		URL dohvati = new URL(url);
		URLConnection connection = dohvati.openConnection();
		InputStream input = connection.getInputStream();
		byte[] buffer = new byte[4096];
		int n;
		OutputStream output = new FileOutputStream(file);
		while ((n = input.read(buffer)) != -1) {
		    output.write(buffer, 0, n);
		}
		output.close();
		unzip("baza/baza.zip"); 
		return true;

	}
	private void unzip(String fileZip) throws IOException {
		File source = new File("baza/baza.sqlite3");
		File dest = new File("baza/baza.sqlite3.backup");
		File arhiva = new File(fileZip);
		if (Files.exists(source.toPath())) {
			if (Files.exists(dest.toPath())) 
				Files.copy(source.toPath(), dest.toPath(), StandardCopyOption.REPLACE_EXISTING);
			else
				Files.copy(source.toPath(), dest.toPath());
		}
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(fileZip));
        ZipEntry zipEntry = zis.getNextEntry();
        while(zipEntry != null){
            String fileName = zipEntry.getName();
            File newFile = new File("baza/" + fileName);
            FileOutputStream fos = new FileOutputStream(newFile);
            int len;
            while ((len = zis.read(buffer)) > 0) {
                fos.write(buffer, 0, len);
            }
            fos.close();
            zipEntry = zis.getNextEntry();
        }
        zis.closeEntry();
        zis.close();
        Files.deleteIfExists(arhiva.toPath());
	}

	/**
	 * Operacija koja se izvršava u posebnoj dretvi u pozadini
	 */
	@Override
	protected Boolean doInBackground() throws Exception {
		return download();
	}
	/**
	 * Po završetku izvođenje operacije u posebnoj dretvi u pozadini
	 * @exception	InterruptedException
	 * @exception	ExecutionException
	 */
	protected void done() {
		try {
			get();
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
