package hr.unidu.pr.projekt.pokazni.postavke;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JOptionPane;
/**
 * Klasa upravljača za evidenciju postavki aplikacije
 */
public class PostavkeUpravljac {
	private PostavkeForma forma;
	private PostavkeModel model;
	private Map<String, String> postavke;

	/**
	 * Konstruktor aplikacije. Otvara formu i popunjava podacima.
	 * @throws Exception
	 */
	public PostavkeUpravljac()  {
		model = new PostavkeModel();
		try {
			postavke = model.dohvatiPostavke();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(forma.getEkran(),
					"Greška: " + e.getMessage(),
					"Greška",
				    JOptionPane.ERROR_MESSAGE);
			return;
		}
		otvoriPostavke();
	}

	private void otvoriPostavke() {
		if (forma == null) {
			forma = new PostavkeForma();
		}
		forma.getUrlHimne().setText(postavke.get("url_himne"));
		forma.getUrlMape().setText(postavke.get("url_mape"));
		forma.getUrlDetalji().setText(postavke.get("url_detalji"));
		forma.getUrlBazaDown().setText(postavke.get("url_baza_down"));
		forma.getUrlBazaUp().setText(postavke.get("url_baza_up"));
		dodajListenere();
		forma.getEkran().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		forma.getEkran().setVisible(true);
	}

	private void dodajListenere() {
		forma.getBtnSpremi().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				promijeniPostavke();
			}
		});
		forma.getBtnKraj().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				forma.getEkran().setVisible(false);
			}
		});
	}

	private void promijeniPostavke() {
			try {
				if (promjenaPostavki()) {
				model.spremiPostavke(postavke);
				JOptionPane.showMessageDialog(forma.getEkran(), "Izmjena URL-ova je uspješna!");
				} else {
					JOptionPane.showMessageDialog(forma.getEkran(),
							"Novi URL-ovi su jednaki starima!\nIzmjena URL-ova nije potrebna!");
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				JOptionPane.showMessageDialog(forma.getEkran(),
				"Greška: " + e.getMessage(),
				"Greška",
			    JOptionPane.ERROR_MESSAGE);
			}
			
		
	}

	private boolean promjenaPostavki() {
		boolean razlika = false;
		if (!forma.getUrlMape().getText().equals(postavke.get("url_mape"))) {
			postavke.put("url_mape", forma.getUrlMape().getText());
			razlika = true;
		}
		if (!forma.getUrlHimne().getText().equals(postavke.get("url_himne"))) {
			postavke.put("url_himne", forma.getUrlHimne().getText());
			razlika = true;
		}
		if (!forma.getUrlDetalji().getText().equals(postavke.get("url_detalji"))) {
			postavke.put("url_detalji", forma.getUrlDetalji().getText());
			razlika = true;
		}
		if (!forma.getUrlBazaDown().getText().equals(postavke.get("url_baza_down"))) {
			postavke.put("url_baza_down", forma.getUrlBazaDown().getText());
			razlika = true;
		}
		if (!forma.getUrlBazaUp().getText().equals(postavke.get("url_baza_up"))) {
			postavke.put("url_baza_up", forma.getUrlBazaUp().getText());
			razlika = true;
		}
		
		return razlika;
	}
}
