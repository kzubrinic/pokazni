package hr.unidu.pr.projekt.pokazni;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.util.Map;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import hr.unidu.pr.projekt.pokazni.drzave.PregledDrzavaUpravljac;
import hr.unidu.pr.projekt.pokazni.postavke.DownloadBaze;
import hr.unidu.pr.projekt.pokazni.postavke.PostavkeUpravljac;
import hr.unidu.pr.projekt.pokazni.postavke.UploadBaze;
import hr.unidu.pr.projekt.pokazni.alati.MojMp3;
import hr.unidu.pr.projekt.pokazni.alati.SviracGlazbe;
import hr.unidu.pr.projekt.pokazni.alati.Zapis;

/**
 * Glavni upravljač aplikacije
 */
public class GlavniUpravljac {
	private GlavnaForma pogled;
	private GlavniModel model;
	private PregledDrzavaUpravljac prUpravljac;
	private String himna;
	private SviracGlazbe m;
	private boolean started = false;
	private Zapis z;
	// private final String himneUrl = "http://www.nationalanthems.info/";
	// private final String pocMape="https://www.google.com/maps/place/";
	private SimpleSwingBrowser browser;
	private Map<String, String> postavke;

	/**
	 * Getter metoda
	 * @return	vraća glavnu formu aplikacije (View dio)
	 */
	public GlavnaForma getPogled() {
		return pogled;
	}
	
	/**
	 * Getter metoda
	 * @return	vraća glavni model aplikacije (Model dio)
	 */
	public GlavniModel getModel() {
		return model;
	}

	/**
	 * Konstruktor aplikacije koji pokreće aplikaciju.
	 * Poziva se iz main metode klase Drzave.
	 * provjerava bazu, po potrebi download postavke.
	 * Otvara glavnu formu, popunjava i inicijalizira 
	 * sve elemente grafičkog sučelja.
	 * @throws	Exception
	 */
	public GlavniUpravljac() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					provjeriBazu();
					pogled = new GlavnaForma();
					pripremiGlavniEkran();
					napuniListenere();
					pogled.podesiVelicinu();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Metoda popunjava glavni ekran podacima dohvaćenima iz Modela.
	 * Dohvaća postavke aplikacije pročitane iz baze.
	 * @throws FileNotFoundException
	 */
	public void pripremiGlavniEkran() throws FileNotFoundException {
		model = new GlavniModel();
		postavke = model.getPostavke();
		napuniGuiModele();
	}
	
	private void provjeriBazu() {
		// postoji li lokalna baza
		File source = new File("baza/baza.sqlite3");
		if(!Files.exists(source.toPath())) { 
		// dohvati bazu s weba
			downloadBaze(true, true);
			if(!Files.exists(source.toPath())) {
				JOptionPane.showMessageDialog(null, "Greška dohvata baze!\n", "Greška dohvata baze", JOptionPane.ERROR_MESSAGE);
				System.exit(-1);
			}
		}
					
	}

	private void napuniGuiModele() {
		pogled.getPopisDrzava().setModel(new DrzaveListModel<String>(model.getDrzave()));
		pogled.getPopisDrzava().setSelectedIndex(0);
		napuniFormu(model.getDrzave().get(0));
	}

	private void napuniListenere() {
		pogled.getPopisDrzava().addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				String koji = pogled.getPopisDrzava().getSelectedValue();
				if (koji != null) {
					napuniFormu(koji);
				}
			}
		});

		pogled.getFilterDrzave().getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
				filtrirajListu();
			}

			public void removeUpdate(DocumentEvent e) {
				filtrirajListu();
			}

			public void insertUpdate(DocumentEvent e) {
				filtrirajListu();
			}

		});

		pogled.getBtnSviraj().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pokreniHimnu();
			}
		});

		pogled.getBtnKraj().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				zaustaviHimnu();
			}
		});

		pogled.getBtnMapaDrzave().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prikaziMapuDrzave();
			}
		});
		
		pogled.getBtnMapaGrada().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prikaziMapuGrada();
			}
		});
		
		pogled.getBtnDetaljiDrzave().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prikaziDetaljeDrzave();
			}
		});

		pogled.getMenuItemAdminIzlaz().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

		pogled.getMenuItemoProgramu().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prikazioProgramu();
			}
		});

		pogled.getMenuItemAdminDrzave().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuAzurirajDrzave();
			}
		});
		
		pogled.getMenuItemUploadBaze().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				uploadBaze();
			}
		});
		
		pogled.getMenuItemDownloadBaze().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				downloadBaze(false, false);
			}
		});
		
		pogled.getMenuItemDownloadIzvorneBaze().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				downloadBaze(true, false);
			}
		});
		
		pogled.getMenuItemAdminPostavke().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				azurirajPostavke();
			}
		});
		
		pogled.getGlavniToolbar().getTipkaToolbarAdminDrzave().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				menuAzurirajDrzave();
			}
		});
		
		pogled.getGlavniToolbar().getTipkaToolbarAdminPostavke().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				azurirajPostavke();
			}
		});
		
		pogled.getGlavniToolbar().getTipkaToolbarPomoc().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				prikazioProgramu();
			}
		});
	}

	private void napuniFormu(String koji) {
		z = model.getZapis(koji);
		pogled.getNazivDrzave().setText(z.getNaziv());
		pogled.getSluzbeniNaziv().setText(z.getPuniNaziv());
		pogled.getGlavniGrad().setText(z.getGlavniGrad());
		pogled.getLblSlikaZastave().setIcon(new ImageIcon("zastave/" + z.getZastava()));
		pogled.getLblSlikaDrzave().setIcon(new ImageIcon("slike/" + z.getSlika()));

		if (z.getKratica() == null) {
			himna = null;
		} else {
			String urlHimne = postavke.get("url_himne");
			himna = urlHimne + z.getHimna();
		}
		zaustaviHimnu();
	}

	private void zaustaviHimnu() {
		if (!(m == null)) {
			m.prekiniGlazbu();
			m = null;
		}
		started = false;
		if (himna == null)
			pogled.getBtnSviraj().setEnabled(false);
		else
			pogled.getBtnSviraj().setEnabled(true);
		pogled.getBtnKraj().setEnabled(false);
	}

	private void pokreniHimnu() {
		if (!started) {
			if (!(himna == null)) {
				m = new MojMp3(himna);
				m.pokreniGlazbu();
			}
			started = true;
		}
		pogled.getBtnSviraj().setEnabled(false);
		pogled.getBtnKraj().setEnabled(true);
	}

	private void prikaziMapuDrzave() {
		String urlMape = postavke.get("url_mape") + pogled.getNazivDrzave().getText();
		prikaziWebStranicu(urlMape);
	}

	private void prikaziMapuGrada() {
		String urlMape = postavke.get("url_mape") + pogled.getGlavniGrad().getText() + ",+" + pogled.getNazivDrzave().getText();
		prikaziWebStranicu(urlMape);
	}
	
	private void prikaziDetaljeDrzave() {
		String urlDetalji = postavke.get("url_detalji");
		String url = urlDetalji + z.getDetalji() + "/";
		prikaziWebStranicu(url);
}

	private void prikaziWebStranicu(final String url) {
				if (browser == null)
					browser = new SimpleSwingBrowser(url);
				else {
					browser.loadURL(url);
					if (!browser.isVisible())
						browser.setVisible(true);
				}
	}
 
	private void filtrirajListu() {
		DrzaveListModel<String> lm = (DrzaveListModel<String>) pogled.getPopisDrzava().getModel();
		lm.filter(pogled.getFilterDrzave().getText());
		String koji = null;
		if (lm.getSize() > 0) {
			koji = lm.getElementAt(0);
		}
		if (koji != null) {
			napuniFormu(koji);
		}
	}

	private void prikazioProgramu() {
		Oprogramu.prikaziInfooProgramu();
	}

	private void azurirajPostavke() {
		new PostavkeUpravljac();
	}
	
	private void menuAzurirajDrzave() {
		azurirajDrzave();
		try {
			pripremiGlavniEkran();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
	}
	
	private void azurirajDrzave() {
		if (prUpravljac == null)
			prUpravljac = new PregledDrzavaUpravljac(this);
		prUpravljac.otvoriPregled();
	}
	
	private void uploadBaze() {
		UploadBaze ub = new UploadBaze(postavke);
		try {
            ub.execute();
            if (ub.get())
            	JOptionPane.showMessageDialog(null, "Upload baze je uspješan!");
            else
            	JOptionPane.showMessageDialog(null, "Greška uploada baze!", "Greška uploada", JOptionPane.ERROR_MESSAGE);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Greška uploada baze!\n"+e.getMessage(), "Greška uploada", JOptionPane.ERROR_MESSAGE);
        }
	
	}
	private void downloadBaze(boolean izvorna, boolean tiho) {
		DownloadBaze ub = new DownloadBaze(postavke, izvorna);
		try {
            ub.execute();
            if (ub.get()) {
            	if (!tiho)
            		JOptionPane.showMessageDialog(null, "Download baze je uspješan!");
            	else
            		System.out.println("Download baze je uspješan!");
            	pripremiGlavniEkran();
            }
            else {
            	if (!tiho)
            		JOptionPane.showMessageDialog(null, "Greška downloada baze!", "Greška downloada", JOptionPane.ERROR_MESSAGE);
            	else
            		System.out.println("Greška downloada baze!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Greška downloada baze!\n"+e.getMessage(), "Greška uploada", JOptionPane.ERROR_MESSAGE);
        }
	}
}
