package hr.unidu.pr.projekt.pokazni.drzave;

import java.util.List;
import javax.swing.table.AbstractTableModel;

import hr.unidu.pr.projekt.pokazni.alati.Zapis;
/**
 * Model pregleda država
 */
public class PregledDrzavaModel extends AbstractTableModel{
	private List<Zapis> lista;
    private String[] columns ={"Id", "Naziv","Puni naziv", "Naziv (en)", 
    		"Puni naziv (en)", "Glavni grad", "Kratica", "Himna", 
    		"Zastava", "Slika", "URL detalja"}; 
    
    /**
     * Setter metoda
     * @param s	lista zapisa za popunjavanje modela tablice
     */
    public PregledDrzavaModel(List<Zapis> s){
        lista = s;
    }
    
    /**
     * Getter metoda
     * @return	vraća broj redaka u tablici
     */
    @Override
    public int getRowCount() {
        return lista.size();
    }

    /**
     * Getter metoda
     * @return	vraća broj stupaca u tablici
     */
    @Override
    public int getColumnCount() {
        return columns.length;
    }

    /**
     * Getter metoda
     * @param r	broj retka	
     * @param s	broj stupca
     * @return	objekt koji se nalazi u retku r i stupcu s
     */
    @Override
    public Object getValueAt(int r, int s) {
        Zapis st = lista.get(r);
        switch(s) {
        case 0:
        	return st.getId();
        case 1:
        	return st.getNaziv();
        case 2:
        	return st.getPuniNaziv();
        case 3:
        	return st.getEnNaziv();
        case 4:
        	return st.getEnPuniNaziv();
        case 5:
        	return st.getGlavniGrad();
        case 6:
        	return st.getKratica();
        case 7:
        	return st.getHimna();
        case 8:
        	return st.getZastava();
        case 9:
        	return st.getSlika();
        case 10:
        	return st.getDetalji();
        default:
        	return st.getNaziv();
        }
    }
    
    /**
     * Getter metoda
     * @param col	broj stupca
     * @return	vraća klasu podatka u stupcu col
     */
    @Override
    public Class<?> getColumnClass(int col) {
    	if (col == 0)
    		return Integer.class;
    	else
           return String.class;
    }
    
    /**
     * Getter metoda
     * @param col	broj stupca
     * @return	vraća naziv stupca col
     */
    @Override
    public String getColumnName(int col) {
        return columns[col] ;
      }
    
    /**
     * Briše redak iz tablice
     * @param z	zapis koji se briše iz tablice
     */
    public void deleteRow(Zapis z) {
    	lista.remove(z);
    }
    
    /**
     * Mijenja redak u tablici
     * @param row	broj retka
     * @param z	novi zapis u retku row
     */
    public void updateRow(int row, Zapis z) {
    	lista.set(row, z);
    }
    
    /**
     * Dodaje novi redak u tablici
     * @param z	novi zapis koji se dodaje u tablicu
     */
    public void addRow(Zapis z) {
    	lista.add(z);
    }

}
