package hr.unidu.pr.projekt.pokazni.drzave;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;
/**
 * Pomoćna klasa za sortiranje redaka u tablici država
 */
public class PregledDrzavaRowComparator implements Comparator {

	/**
	 * Komparator za sortiranje u tablici država
	 * @param o1	objekt koji se uspoređuje
	 * @param o2	objekt koji se uspoređuje
	 * @return	0, pozitivan broj ili negativan broj ovisno o odnosu dva podatka koji se uspoređuju.  
	 */
	@Override
	public int compare(Object o1, Object o2) {
		if (o1 instanceof Integer) {
			return (Integer)o1 - (Integer)o2;
		}
		else {
			Locale locale = new Locale("hr", "HR");
			Collator col = Collator.getInstance(locale);
			return col.compare((String)o1, (String)o2);
		}
	}

}
