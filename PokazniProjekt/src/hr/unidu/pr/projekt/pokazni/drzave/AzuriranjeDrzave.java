package hr.unidu.pr.projekt.pokazni.drzave;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Dialog;

import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JTextField;

/**
 * Forma ažuriranja države

 */
public class AzuriranjeDrzave extends JDialog {

	private static final long serialVersionUID = 9199519539362043528L;
	private JPanel contentPane;
	private JTextField tNaziv;
	private JTextField tPuniNaziv;
	private JTextField tNazivEn;
	private JTextField tPuniNazivEn;
	private JTextField tGlavniGrad;
	private JTextField tKratica;
	private JTextField tHimna;
	private JTextField tZastava;
	private JTextField tSlika;
	private JTextField tDetalji;
	private JTextField tId;
	private JButton btnSpremi, btnKraj;
	private JPanel panel;

	/**
	 * Getter metoda
	 * @return	glavni kontejner forme
	 */
	public JPanel getContentPane() {
		return contentPane;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži naziv 
	 */
	public JTextField gettNaziv() {
		return tNaziv;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži puni naziv 
	 */
	public JTextField gettPuniNaziv() {
		return tPuniNaziv;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži naziv na engleskom
	 */
	public JTextField gettNazivEn() {
		return tNazivEn;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži puni naziv na engleskom 
	 */
	public JTextField gettPuniNazivEn() {
		return tPuniNazivEn;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži glavni grad 
	 */
	public JTextField gettGlavniGrad() {
		return tGlavniGrad;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži ISO kraticu 
	 */
	public JTextField gettKratica() {
		return tKratica;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži naziv datoteke himne 
	 */
	public JTextField gettHimna() {
		return tHimna;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži naziv datoteke zastave
	 */
	public JTextField gettZastava() {
		return tZastava;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži naziv datoteke slike
	 */
	public JTextField gettSlika() {
		return tSlika;
	}
	
	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži oznaku detalja 
	 */
	public JTextField gettDetalji() {
		return tDetalji;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži id države 
	 */
	public JTextField gettId() {
		return tId;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži gumb za spremanje 
	 */
	public JButton getBtnSpremi() {
		return btnSpremi;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži gumb za završetak rada 
	 */
	public JButton getBtnKraj() {
		return btnKraj;
	}

	/**
	 * Konstruktor priprema formu
	 */
	public AzuriranjeDrzave() {
		setTitle("Država");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 523, 328);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new MigLayout("", "[][grow][]", "[][][][][][][][][][][]"));
		
		JLabel lblId = new JLabel("Id");
		panel.add(lblId, "cell 0 0,alignx trailing");
		
		tId = new JTextField();
		tId.setEditable(false);
		panel.add(tId, "cell 1 0");
		tId.setColumns(10);
		
		JLabel lblNaziv = new JLabel("Naziv");
		panel.add(lblNaziv, "cell 0 1,alignx trailing");
		
		tNaziv = new JTextField();
		panel.add(tNaziv, "cell 1 1,aligny baseline");
		tNaziv.setColumns(20);
		
		JLabel lblPuniNaziv = new JLabel("Puni naziv");
		panel.add(lblPuniNaziv, "cell 0 2,alignx trailing");
		
		tPuniNaziv = new JTextField();
		panel.add(tPuniNaziv, "cell 1 2,growx");
		tPuniNaziv.setColumns(10);
		
		JLabel lblNaziven = new JLabel("Naziv (en)");
		panel.add(lblNaziven, "cell 0 3,alignx trailing");
		
		tNazivEn = new JTextField();
		panel.add(tNazivEn, "cell 1 3");
		tNazivEn.setColumns(20);
		
		JLabel lblPuniNaziven = new JLabel("Puni naziv (en)");
		panel.add(lblPuniNaziven, "cell 0 4,alignx trailing");
		
		tPuniNazivEn = new JTextField();
		panel.add(tPuniNazivEn, "cell 1 4,growx");
		tPuniNazivEn.setColumns(10);
		
		JLabel lblGlavniGrad = new JLabel("Glavni grad");
		panel.add(lblGlavniGrad, "cell 0 5,alignx trailing");
		
		tGlavniGrad = new JTextField();
		panel.add(tGlavniGrad, "cell 1 5,growx");
		tGlavniGrad.setColumns(10);
		
		JLabel lblKratica = new JLabel("Kratica");
		panel.add(lblKratica, "cell 0 6,alignx trailing");
		
		tKratica = new JTextField();
		panel.add(tKratica, "cell 1 6");
		tKratica.setColumns(4);
		
		JLabel lblHimna = new JLabel("Himna");
		panel.add(lblHimna, "cell 0 7,alignx trailing");
		
		tHimna = new JTextField();
		panel.add(tHimna, "cell 1 7");
		tHimna.setColumns(10);
		
		JLabel lblZastava = new JLabel("Zastava");
		panel.add(lblZastava, "cell 0 8,alignx trailing");
		
		tZastava = new JTextField();
		panel.add(tZastava, "cell 1 8,growx");
		tZastava.setColumns(10);
		
		JLabel lblSlika = new JLabel("Slika");
		panel.add(lblSlika, "cell 0 9,alignx trailing");
		
		tSlika = new JTextField();
		panel.add(tSlika, "cell 1 9,growx");
		tSlika.setColumns(10);
		
		JLabel lblDetalji = new JLabel("URL detalja");
		panel.add(lblDetalji, "cell 0 10,alignx trailing");
		
		tDetalji = new JTextField();
		panel.add(tDetalji, "cell 1 10,growx");
		tDetalji.setColumns(20);

		
		btnSpremi = new JButton("Spremi");
		panel.add(btnSpremi, "cell 1 11");
		
		btnKraj = new JButton("Odustani");
		panel.add(btnKraj, "cell 1 11");
		
		setLocation(350, 400);
		
		setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
	}
	
	/**
	 * Metoda koja inicijalizira i čisti polja forme.
	 */
	public void cistiPolja() {
		Container con = panel;
        for (Component c : con.getComponents()){
            if (c instanceof JTextField) {
                JTextField j = (JTextField)c;
                j.setText("");
            }
        }
	}

}
