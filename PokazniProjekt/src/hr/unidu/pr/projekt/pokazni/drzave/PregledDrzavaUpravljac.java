package hr.unidu.pr.projekt.pokazni.drzave;

import java.awt.Dialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableRowSorter;

import hr.unidu.pr.projekt.pokazni.GlavniUpravljac;
import hr.unidu.pr.projekt.pokazni.alati.Zapis;
import hr.unidu.pr.projekt.pokazni.baza.Baza;
import hr.unidu.pr.projekt.pokazni.baza.SqliteBaza;

/**
 * Upravljač pregleda država
 */
public class PregledDrzavaUpravljac {

	private PregledDrzavaTablica forma;
	private PregledDrzavaModel model;
	private AzuriranjeDrzaveUpravljac azur;
	private GlavniUpravljac glavni;

	/**
	 * Konstruktor klase
	 * @param g	glavni upravljač aplikacije
	 */
	public PregledDrzavaUpravljac(GlavniUpravljac g) {
		glavni = g;
	}

	/**
	 * Getter metoda
	 * @return	vraća glavni upravljač aplikacije
	 */
	public GlavniUpravljac getGlavni() {
		return glavni;
	}

	/**
	 * Metoda otvara formu s pregledom država i popunjava model 
	 * @throws	Exception
	 */
	public void otvoriPregled() {
		if (forma == null) {
			forma = new PregledDrzavaTablica();
		}
		try {
			napuniModel();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(forma.getEkran(),
					"Greška: " + e.getMessage(),
					"Greška",
				    JOptionPane.ERROR_MESSAGE);
			return;
		}
		forma.podesiSirinuStupaca();
		forma.getEkran().pack();
		dodajListenere();
		forma.getEkran().setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
		forma.getEkran().setVisible(true);
	}

	private void dodajListenere() {
		forma.getTablica().addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				// obradiCeliju(forma.getTablica());
				if (e.getClickCount() == 2) {
					izmijeniDrzavu(forma.getTablica());
				}
			}
		});

		forma.getBtnNova().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				novaDrzava(forma.getTablica());
			}
		});

		forma.getBtnDohvati().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				izmijeniDrzavu(forma.getTablica());
			}
		});
		
		forma.getBtnUvid().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				izmijeniDrzavu(forma.getTablica(), true);
			}
		});

		forma.getBtnBrisi().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				obrisiDrzavu(forma.getTablica());
			}
		});

		forma.getBtnKraj().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				forma.getEkran().setVisible(false);
			}
		});
	}

	private void obrisiDrzavu(JTable tablica) {
		if (tablica.getSelectedRow() < 0) {
			JOptionPane.showMessageDialog(null, "Morate izabrati redak s državom koju želite obrisati!");
			return;
		}

		int brSr = tablica.convertRowIndexToModel(tablica.getSelectedRow());
		String izabranaDrzava = (String) model.getValueAt(brSr, 1);
		int id = (Integer) model.getValueAt(brSr, 0);

		Zapis z = new Zapis((String) model.getValueAt(brSr, 1), (String) model.getValueAt(brSr, 2),
				(String) model.getValueAt(brSr, 3), (String) model.getValueAt(brSr, 4),
				(String) model.getValueAt(brSr, 5), (String) model.getValueAt(brSr, 6),
				(String) model.getValueAt(brSr, 7), (String) model.getValueAt(brSr, 8),
				(String) model.getValueAt(brSr, 9), (String) model.getValueAt(brSr, 10),
				(Integer) model.getValueAt(brSr, 0));

		Object[] options = { "Da", "Ne" };
		int n = JOptionPane.showOptionDialog(null,
				"Želite li obrisati zapis s ID-jem " + id + "\n" + "drzava:" + izabranaDrzava + "?", "Brisanje države",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, // do not use a custom Icon
				options, // the titles of buttons
				options[0]); // default button title
		// model.azuriranjeZapisa(z);
		// poruka = "Izmjena zapisa države je uspješna!";
		try {
		if (n == 0) { // brisanje
			AzuriranjeDrzaveModel m = new AzuriranjeDrzaveModel();
			m.brisanjeZapisa(id);
			model.deleteRow(z);
			model.fireTableDataChanged();
			glavni.getModel().procitajZapise();
			JOptionPane.showMessageDialog(null, "Brisanje uspješno!");
		}

			glavni.pripremiGlavniEkran();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(forma.getEkran(),
					"Greška: " + e.getMessage(),
					"Greška",
				    JOptionPane.ERROR_MESSAGE);
		}

	}

	private void novaDrzava(JTable t) {
		//System.out.println("AAA "+ t.getSelectedRow()+ " BBB:" + t.convertRowIndexToModel(t.getSelectedRow()));
		if (azur == null)
			azur = new AzuriranjeDrzaveUpravljac(this);
		azur.napuniModel(null, forma.getTablica(), -1);
		azur.getForma().getBtnSpremi().setEnabled(true);
		azur.getForma().setTitle("Unos nove države");
		azur.getForma().setVisible(true);
	}

	private void izmijeniDrzavu(JTable t)  {
		//System.out.println("A "+ t.getSelectedRow());
		AbstractTableModel model = (AbstractTableModel) t.getModel();
		if (t.getSelectedRow() < 0) {
			JOptionPane.showMessageDialog(null, "Morate izabrati redak države u koju želite dobiti uvid!");
			return;
		}
		
		int red = t.convertRowIndexToModel(t.getSelectedRow());
		if (azur == null)
			azur = new AzuriranjeDrzaveUpravljac(this);
		azur.napuniModel(model, forma.getTablica(), red);
		azur.getForma().setTitle("Izmjena izabrane države");
		azur.getForma().getBtnSpremi().setEnabled(true);
		azur.getForma().setVisible(true);

		try {
			napuniModel();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(forma.getEkran(),
					"Greška: " + e.getMessage(),
					"Greška",
				    JOptionPane.ERROR_MESSAGE);
		}
	}
	private void izmijeniDrzavu(JTable t, boolean samoUvid)  {
		//System.out.println("AA "+ t.getSelectedRow()+ " BB:" + t.convertRowIndexToModel(t.getSelectedRow()));
		if (t.getSelectedRow() < 0) {
			JOptionPane.showMessageDialog(null, "Morate izabrati redak države u koju želite dobiti uvid!");
			return;
		}
		AbstractTableModel model = (AbstractTableModel) t.getModel();
		int red = t.convertRowIndexToModel(t.getSelectedRow());
		if (azur == null)
			azur = new AzuriranjeDrzaveUpravljac(this);
		azur.napuniModel(model, forma.getTablica(), red);
		azur.getForma().setTitle("Uvid u izabranu državu");
		//azur.getForma().setModal(true);
		
		try {
			napuniModel();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(forma.getEkran(),
					"Greška: " + e.getMessage(),
					"Greška",
				    JOptionPane.ERROR_MESSAGE);
		}
		azur.getForma().getBtnSpremi().setEnabled(false);
		azur.getForma().setVisible(true);
	}

	private void napuniModel() throws Exception {
		List<Zapis> zapisi = new ArrayList<>();
		procitajZapise(zapisi);
		model = new PregledDrzavaModel(zapisi);
		forma.getTablica().setModel(model);
		TableRowSorter<PregledDrzavaModel> trs = new TableRowSorter<>(model);
		// Sve stupce osim provog sortira po abecedi vodeći računa o kodnoj stranici
		for (int i = 1; i < 10; ++i) {
			trs.setComparator(i, new PregledDrzavaRowComparator());
		}
		forma.getTablica().setRowSorter(trs);
	}

	private void procitajZapise(List<Zapis> st) throws Exception {
		Baza b = new SqliteBaza();
		List<String> drzave = new ArrayList<String>();
		Map<String, Zapis> zapisi = b.citajSve(drzave);

		for (Map.Entry<String, Zapis> entry : zapisi.entrySet()) {
			st.add(entry.getValue());
		}

		Collections.sort(st);
	}
}
