package hr.unidu.pr.projekt.pokazni.drzave;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.BorderLayout;
import java.awt.Dimension;
/**
 * Forma koja prikazuje tablicu država
 */
public class PregledDrzavaTablica {

	private JPanel cp, cp1;
	private JDialog ekran;
	private JTable tablica;
	private JButton btnNova, btnDohvati, btnBrisi, btnUvid, btnKraj;
	
	/**
	 * Getter metoda
	 * @return	vraća formu pregleda država
	 */
	public JDialog getEkran() {
		return ekran;
	}

	/**
	 * Getter metoda
	 * @return	vraća Swing komponentu tablice država
	 */
	public JTable getTablica() {
		return tablica;
	}
	
	/**
	 * Getter metoda
	 * @return	vraća Swing komponentu gumba za unos nove države
	 */
	public JButton getBtnNova() {
		return btnNova;
	}
	
	/**
	 * Getter metoda
	 * @return	vraća Swing komponentu gumba za brisanje države
	 */
	public JButton getBtnBrisi() {
		return btnBrisi;
	}
	
	/**
	 * Getter metoda
	 * @return	vraća Swing komponentu gumba za dohvat države
	 */
	public JButton getBtnDohvati() {
		return btnDohvati;
	}
	
	/**
	 * Getter metoda
	 * @return	vraća Swing komponentu gumba za uvid u državu
	 */
	public JButton getBtnUvid() {
		return btnUvid;
	}

	/**
	 * Getter metoda
	 * @return	vraća Swing komponentu gumba za kraj rada
	 */
	public JButton getBtnKraj() {
		return btnKraj;
	}
	
	/**
	 * Konstruktor klase. Otvara formu i popunjava podacima.
	 */
	public PregledDrzavaTablica() {
		ekran = new JDialog();
		ekran.setTitle("Podaci države");
		cp = new JPanel();
		cp1 = new JPanel();
		tablica = new JTable();
		tablica.setAutoCreateRowSorter(false);
		
		tablica.setPreferredScrollableViewportSize(new Dimension(1400, 400));
        tablica.setFillsViewportHeight(true);
        
        JScrollPane pp = new JScrollPane(tablica);
		cp.add(pp);
		ekran.getContentPane().add(cp,BorderLayout.NORTH);
	
		btnNova = new JButton("Nova država");
		cp1.add(btnNova);
	
		btnDohvati = new JButton("Izmjena države");
		cp1.add(btnDohvati);
		
		btnBrisi = new JButton("Brisanje države");
		cp1.add(btnBrisi);
		
		btnUvid = new JButton("Uvid u podatke države");
		cp1.add(btnUvid);
		
		btnKraj = new JButton("Odustani");
		cp1.add(btnKraj);
		
		ekran.getContentPane().add(cp1,BorderLayout.SOUTH);
		ekran.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		ekran.pack();
		ekran.setLocation(240, 250);
	}
	
	/**
	 * Metoda koja podešava širinu stupaca tablice ovisno o sadržaju
	 */
	public void podesiSirinuStupaca() {
        tablica.getColumnModel().getColumn(0).setPreferredWidth(2);
        tablica.getColumnModel().getColumn(1).setPreferredWidth(50);
        tablica.getColumnModel().getColumn(2).setPreferredWidth(100);
        tablica.getColumnModel().getColumn(3).setPreferredWidth(50);
        tablica.getColumnModel().getColumn(4).setPreferredWidth(100);
        tablica.getColumnModel().getColumn(5).setPreferredWidth(100);
        tablica.getColumnModel().getColumn(6).setPreferredWidth(2);
        tablica.getColumnModel().getColumn(7).setPreferredWidth(20);
        tablica.getColumnModel().getColumn(8).setPreferredWidth(20);
        tablica.getColumnModel().getColumn(9).setPreferredWidth(20);
        tablica.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
	}
}
