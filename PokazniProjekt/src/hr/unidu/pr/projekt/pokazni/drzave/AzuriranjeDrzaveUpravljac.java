package hr.unidu.pr.projekt.pokazni.drzave;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.TableModel;

import hr.unidu.pr.projekt.pokazni.DrzaveListModel;
import hr.unidu.pr.projekt.pokazni.alati.Zapis;

/**
 * Upravljač za ažuriranje države
 */
public class AzuriranjeDrzaveUpravljac {
	private AzuriranjeDrzave forma;
	private AzuriranjeDrzaveModel model;
	private PregledDrzavaUpravljac pregledUpravljac;
	private JTable tablica;
	private int izabraniRed;
	
	/**
	 * Konstruktor
	 * @param p	upravljač pregleda država
	 */
	public AzuriranjeDrzaveUpravljac(PregledDrzavaUpravljac p) {
		pregledUpravljac = p;
		otvoriFormu();
		pripremiModel();
	}

	/**
	 * Getter metoda
	 * @return	vraća formu za ažuriranje države
	 */
	public AzuriranjeDrzave getForma() {
		return forma;
	}

	/**
	 * Getter metoda
	 * @return	vraća upravljač ažuriranja države
	 */
	public PregledDrzavaUpravljac getPregledUpravljac() {
		return pregledUpravljac;
	}

	private void pripremiModel() {
		model = new AzuriranjeDrzaveModel();
	}

	private void otvoriFormu() {
		if (forma == null) {
			forma = new AzuriranjeDrzave();
		}
		forma.pack();
		dodajListenere();
	}

	private void dodajListenere() {
		forma.getBtnSpremi().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				azuriranjeZapisa();
			}

		});

		forma.getBtnKraj().addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				forma.setVisible(false);
			}
		});

	}

	private void azuriranjeZapisa() {
		String poruka;
		try {
			if (svaPoljaNisuPopunjena()) {
				forma.gettNaziv().requestFocusInWindow();
				JOptionPane.showMessageDialog(null, "Morate popuniti sva polja!");
				return;
			}
			PregledDrzavaModel mp = (PregledDrzavaModel) tablica.getModel();

			Zapis z = new Zapis(forma.gettNaziv().getText(), forma.gettPuniNaziv().getText(),
					forma.gettNazivEn().getText(), forma.gettPuniNazivEn().getText(), forma.gettGlavniGrad().getText(),
					forma.gettKratica().getText(), forma.gettHimna().getText(), forma.gettZastava().getText(),
					forma.gettSlika().getText(), forma.gettDetalji().getText(),
					Integer.parseInt(forma.gettId().getText()));
			if (z.getId() > 0) {
				model.azuriranjeZapisa(z);
				mp.updateRow(izabraniRed, z);
				// pregledUpravljac.getGlavni().getModel().procitajZapise();

				poruka = "Izmjena zapisa države je uspješna!";
			} else {
				int id = model.unosZapisa(z);
				z.setId(id);
				forma.gettId().setText(String.valueOf(id));
				poruka = "Unos nove države je uspješan!";
				mp.addRow(z);
				DrzaveListModel<String> dlm = (DrzaveListModel<String>) pregledUpravljac.getGlavni().getPogled()
						.getPopisDrzava().getModel();
				dlm.addElement(z.getNaziv());
				// pregledUpravljac.getGlavni().getModel().procitajZapise();
			}
			mp.fireTableDataChanged();
			JOptionPane.showMessageDialog(null, poruka);
			forma.setVisible(false);

			pregledUpravljac.getGlavni().pripremiGlavniEkran();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Greška: " + e.getMessage(), "Greška", JOptionPane.ERROR_MESSAGE);

		}
	}

	/**
	 * Metoda koja ažurira model pregleda zapisa nakon izmjene jednog zapisa
	 * @param model	model pregleda zapisa
	 * @param t	Swing komponenta pregleda
	 * @param r	redak zapisa koji se mijenja
	 */
	public void napuniModel(TableModel model, JTable t, int r) {
		tablica = t;
		izabraniRed = r;
		if (model == null) {
			forma.cistiPolja();
			forma.gettId().setText("-1");
		} else {
			int brStup = model.getColumnCount();
			// Povezuje indeks retka tablice s indeksom retka u modelu
			// Ako podaci nisu sortirani, indeksi su jednaki!
			int brSr = t.convertRowIndexToModel(t.getSelectedRow());
			for (int j = 0; j < brStup; j++) {
				switch (j) {
				case 0:
					Integer it = (Integer) model.getValueAt(brSr, j);
					forma.gettId().setText(it.toString());
					break;
				case 1:
					forma.gettNaziv().setText((String) model.getValueAt(brSr, j));
					break;
				case 2:
					forma.gettPuniNaziv().setText((String) model.getValueAt(brSr, j));
					break;
				case 3:
					forma.gettNazivEn().setText((String) model.getValueAt(brSr, j));
					break;
				case 4:
					forma.gettPuniNazivEn().setText((String) model.getValueAt(brSr, j));
					break;
				case 5:
					forma.gettGlavniGrad().setText((String) model.getValueAt(brSr, j));
					break;
				case 6:
					forma.gettKratica().setText((String) model.getValueAt(brSr, j));
					break;
				case 7:
					forma.gettHimna().setText((String) model.getValueAt(brSr, j));
					break;
				case 8:
					forma.gettZastava().setText((String) model.getValueAt(brSr, j));
					break;
				case 9:
					forma.gettSlika().setText((String) model.getValueAt(brSr, j));
					break;
				case 10:
					forma.gettDetalji().setText((String) model.getValueAt(brSr, j));
					break;
				}
			}
		}
		forma.gettNaziv().requestFocusInWindow();
	}

	private boolean svaPoljaNisuPopunjena() {
		return forma.gettNaziv().getText().equals("") || forma.gettPuniNaziv().getText().equals("")
				|| forma.gettNazivEn().getText().equals("") || forma.gettPuniNazivEn().getText().equals("")
				|| forma.gettGlavniGrad().getText().equals("") || forma.gettKratica().getText().equals("")
				|| forma.gettHimna().getText().equals("") || forma.gettZastava().getText().equals("")
				|| forma.gettSlika().getText().equals("");
	}
}
