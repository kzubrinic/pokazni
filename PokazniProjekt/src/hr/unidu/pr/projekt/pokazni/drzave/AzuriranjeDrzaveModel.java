package hr.unidu.pr.projekt.pokazni.drzave;

import hr.unidu.pr.projekt.pokazni.alati.Zapis;
import hr.unidu.pr.projekt.pokazni.baza.Baza;
import hr.unidu.pr.projekt.pokazni.baza.SqliteBaza;

/**
 * Model za ažuriranje države
 */
public class AzuriranjeDrzaveModel {
	/**
	 * Metoda koja ažurira zapis države
	 * @param z	zapis države koja se mijenja
	 * @throws Exception
	 */
	public void azuriranjeZapisa(Zapis z) throws Exception {
		Baza b = new SqliteBaza();
		b.updateDrzava(z);
	}
	
	/**
	 * Metoda koja unosi novi zapis države
	 * @param z	zapis države koja se unosi
	 * @return	id novog zapisa države
	 * @throws Exception
	 */
	public int unosZapisa(Zapis z) throws Exception {
		Baza b = new SqliteBaza();
		return b.insertDrzava(z);
	}
	
	/**
	 * Metoda koja briše zapis države
	 * @param id	id države koja se briše
	 * @throws Exception
	 */
	public void brisanjeZapisa(int id) throws Exception {
		Baza b = new SqliteBaza();
		b.deleteDrzava(id);
	}
}
