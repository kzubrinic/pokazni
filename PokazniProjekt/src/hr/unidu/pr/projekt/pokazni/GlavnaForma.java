package hr.unidu.pr.projekt.pokazni;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.FileNotFoundException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import net.miginfocom.swing.MigLayout;

/**
 * Glavna forma aplikacije. sadrži izbornik.
 * U lijevom dijelu prikazuje listu država koja se može filtrirati.
 * U desnom dijelu prikazuje detalje izabrane države. 
 * 
 */
public class GlavnaForma extends JFrame {
	private static final long serialVersionUID = 4835895062329407894L;
	private JPanel contentPane;
	private JTextField nazivDrzave, sluzbeniNaziv, glavniGrad, filterDrzave;
	private JLabel lblSlikaDrzave = new JLabel();
	private JLabel lblSlikaZastave = new JLabel();
	private JButton btnSviraj, btnKraj, btnMapaDrzave, btnMapaGrada, btnDetaljiDrzave;
	private JMenuBar menu;
	private JMenu menu1, menu2, podmenu;
	private JMenuItem menuItemAdminDrzave, menuItemAdminPostavke, menuItemAdminIzlaz, menuItemoProgramu;
	private JMenuItem menuItemUploadBaze, menuItemDownloadBaze, menuItemDownloadIzvorneBaze;
	private JList<String> popisDrzava;
	private GlavnaFormaToolbar glavniToolbar ;

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži glavni kontejner forme
	 */
	public JPanel getContentPane() {
		return contentPane;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži naziv države
	 */
	public JTextField getNazivDrzave() {
		return nazivDrzave;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži službeni naziv države
	 */
	public JTextField getSluzbeniNaziv() {
		return sluzbeniNaziv;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži glavni grad države
	 */
	public JTextField getGlavniGrad() {
		return glavniGrad;
	}
	
	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži filter za filtriranje država u listi
	 */
	public JTextField getFilterDrzave() {
		return filterDrzave;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži labelu sa slikom države
	 */
	public JLabel getLblSlikaDrzave() {
		return lblSlikaDrzave;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži labelu sa slikom zastave
	 */
	public JLabel getLblSlikaZastave() {
		return lblSlikaZastave;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži gumb za početak reprodukcije himne
	 */
	public JButton getBtnSviraj() {
		return btnSviraj;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži gumb za završetak reprodukcije himne
	 */
	public JButton getBtnKraj() {
		return btnKraj;
	}
	
	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži gumb za dohvat mape države
	 */
	public JButton getBtnMapaDrzave() {
		return btnMapaDrzave;
	}
	
	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži gumb za dohvat mape grada
	 */
	public JButton getBtnMapaGrada() {
		return btnMapaGrada;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži gumb za dohvat detalja države
	 */
	public JButton getBtnDetaljiDrzave() {
		return btnDetaljiDrzave;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži listu države
	 */
	public JList<String> getPopisDrzava() {
		return popisDrzava;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži izbornik izlaza
	 */
	public JMenuItem getMenuItemAdminIzlaz() {
		return menuItemAdminIzlaz;
	}
	
	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži izbornik administracije država
	 */
	public JMenuItem getMenuItemAdminDrzave() {
		return menuItemAdminDrzave;
	}
	
	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži izbornik administracije postavki
	 */
	public JMenuItem getMenuItemAdminPostavke() {
		return menuItemAdminPostavke;
	}
	
	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži izbornik pomoći
	 */
	public JMenuItem getMenuItemoProgramu() {
		return menuItemoProgramu;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži izbornik za upload baze
	 */
	public JMenuItem getMenuItemUploadBaze() {
		return menuItemUploadBaze;
	}
	
	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži izbornik za download baze
	 */
	public JMenuItem getMenuItemDownloadBaze() {
		return menuItemDownloadBaze;
	}

	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži izbornik za download izvorne baze
	 */
	public JMenuItem getMenuItemDownloadIzvorneBaze() {
		return menuItemDownloadIzvorneBaze;
	}
	
	/**
	 * Getter metoda
	 * @return	Swing komponentu koja sadrži toolbar na glavnoj formi
	 */
	public GlavnaFormaToolbar getGlavniToolbar() {
		return glavniToolbar;
	}

	/**
	 * Konstruktor koji stvara novu formu država.
	 * @throws FileNotFoundException 
	 */
	public GlavnaForma() throws FileNotFoundException {
		setTitle("Države");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout());
		setContentPane(contentPane);
		
		menu = new JMenuBar();
		setJMenuBar(menu);
		
		menu1 = new JMenu("Administracija");
		menu2 = new JMenu("Pomoć");
		podmenu = new JMenu("Ažuriranje baze");

		menu.add(menu1);
		menu.add(menu2);
		
		menuItemAdminDrzave = new JMenuItem("Administracija država");
		menuItemAdminPostavke = new JMenuItem("Administracija postavki");
		menuItemAdminIzlaz = new JMenuItem("Izlaz");
		menu1.add(menuItemAdminDrzave);
		menu1.add(menuItemAdminPostavke);
		menu1.addSeparator();
		menu1.add(podmenu);
		menu1.addSeparator();
		menu1.add(menuItemAdminIzlaz);
		
		menuItemDownloadBaze = new JMenuItem("Download korisničke baze");
		menuItemUploadBaze = new JMenuItem("Upload korisničke baze");
		menuItemDownloadIzvorneBaze = new JMenuItem("Download izvorne baze");

		podmenu.add(menuItemDownloadBaze);
		podmenu.add(menuItemUploadBaze);
		podmenu.addSeparator();
		podmenu.add(menuItemDownloadIzvorneBaze);
		
		menuItemoProgramu = new JMenuItem("O programu");
		menu2.add(menuItemoProgramu);
		
		popisDrzava = new JList<String>();
		popisDrzava.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		popisDrzava.setVisibleRowCount(15);

		glavniToolbar = new GlavnaFormaToolbar();
		contentPane.add(glavniToolbar, BorderLayout.NORTH);
		
		JScrollPane jp;
		jp = new JScrollPane(popisDrzava);

		filterDrzave = new JTextField(28);
		JPanel lijevo = new JPanel();
		lijevo.setLayout(new BorderLayout());
		lijevo.add(filterDrzave, BorderLayout.NORTH);
		lijevo.add(jp);
		contentPane.add(lijevo, BorderLayout.WEST);

		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(new MigLayout("wrap 2", "[][][]", "[][][][]"));

		JLabel lblNaziv = new JLabel("Naziv");
		panel.add(lblNaziv, "cell 1 0");

		nazivDrzave = new JTextField();
		nazivDrzave.setEditable(false);

		panel.add(nazivDrzave, "cell 2 0");
		nazivDrzave.setColumns(30);

		btnMapaDrzave = new JButton("Mapa države");
		panel.add(btnMapaDrzave, "flowx,cell 3 0");
		
		JLabel lblSluzbeniNaziv = new JLabel("Službeni naziv");
		panel.add(lblSluzbeniNaziv, "cell 1 1");

		sluzbeniNaziv = new JTextField();
		sluzbeniNaziv.setEditable(false);
		panel.add(sluzbeniNaziv, "cell 2 1");
		sluzbeniNaziv.setColumns(30);

		JLabel lblGlavniGrad = new JLabel("Glavni grad");
		panel.add(lblGlavniGrad, "cell 1 2");

		glavniGrad = new JTextField();
		glavniGrad.setEditable(false);
		panel.add(glavniGrad, "cell 2 2");
		glavniGrad.setColumns(30);
		
		btnMapaGrada = new JButton("Mapa grada");
		panel.add(btnMapaGrada, "flowx,cell 3 2");

		JLabel lblZastava = new JLabel("Zastava");
		panel.add(lblZastava, "cell 1 3");

		panel.add(lblSlikaZastave, "flowx,cell 2 3");

		JLabel lblHimna = new JLabel("Himna");
		panel.add(lblHimna, "cell 1 4");

		btnSviraj = new JButton();
		btnSviraj.setIcon(new ImageIcon("ikone/start.png"));
	    panel.add(btnSviraj, "flowx,cell 2 4");

		btnKraj = new JButton();
		btnKraj.setIcon(new ImageIcon("ikone/stop.png"));
		panel.add(btnKraj, "cell 2 4");
		
		btnDetaljiDrzave = new JButton("Detaljniji podaci države");
		panel.add(btnDetaljiDrzave, "cell 2 5");

		lblSlikaDrzave.setMinimumSize(new Dimension(280, 320));
		lblSlikaDrzave.setMaximumSize(new Dimension(280, 320));
		contentPane.add(lblSlikaDrzave, BorderLayout.EAST);

		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
	}
	
	/**
	 * Metoda koja podešava veličinu forme nakon što se popuni komponentama
	 */
	public void podesiVelicinu() {
		pack();
		setLocation(100, 100);
		setVisible(true);
	}
}