package hr.unidu.pr.projekt.pokazni.alati;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * Podatkovna DAO klasa zapisa
 */
public class Zapis implements Comparable<Zapis>, Comparator<Zapis> {
	private String naziv, puniNaziv, enNaziv, enPuniNaziv, glavniGrad, kratica, himna, zastava, slika, detalji;
	private int id;
	/**
	 * Konstruktor koji stvara novi zapis
	 * @param naziv	naziv države
	 * @param puniNaziv	puni naziv države
	 * @param enNaziv	naziv države na engleskom jeziku
	 * @param enPuniNaziv	puni naziv države na engleskom jeziku
	 * @param glavniGrad	glavni grad države
	 * @param kratica	ISO 2-slovna kratica države
	 * @param himna	naziv datoteke himne države
	 * @param zastava	naziv datoteke zastave države
	 * @param slika	naziv datoteke slike države
	 * @param detalji	oznaka za pristup detaljima države
	 * @param id	id države
	 */
	public Zapis(String naziv, String puniNaziv, String enNaziv, String enPuniNaziv, String glavniGrad, String kratica,
			String himna, String zastava, String slika, String detalji, int id) {
		super();
		this.naziv = naziv;
		this.puniNaziv = puniNaziv;
		this.enNaziv = enNaziv;
		this.enPuniNaziv = enPuniNaziv;
		this.glavniGrad = glavniGrad;
		this.kratica = kratica;
		this.himna = himna;
		this.zastava = zastava;
		this.slika = slika;
		this.detalji = detalji;
		this.id = id;
	}

	/**
	 * Getter metoda
	 * @return	naziv države
	 */
	public String getNaziv() {
		return naziv;
	}

	/**
	 * Setter metoda
	 * @param naziv	naziv države
	 */
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	/**
	 * Getter metoda
	 * @return	puni naziv države
	 */
	public String getPuniNaziv() {
		return puniNaziv;
	}

	/**
	 * Setter metoda
	 * @param puniNaziv	puni naziv države
	 */
	public void setPuniNaziv(String puniNaziv) {
		this.puniNaziv = puniNaziv;
	}

	/**
	 * Getter metoda
	 * @return	naziv države na engleskom jeziku
	 */
	public String getEnNaziv() {
		return enNaziv;
	}

	/**
	 * Setter metoda
	 * @param enNaziv	naziv države na engleskom
	 */
	public void setEnNaziv(String enNaziv) {
		this.enNaziv = enNaziv;
	}

	/**
	 * Getter metoda
	 * @return	puni naziv države na engleskom jeziku
	 */
	public String getEnPuniNaziv() {
		return enPuniNaziv;
	}

	/**
	 * Setter metoda
	 * @param enPuniNaziv	puni naziv države na engleskom
	 */
	public void setEnPuniNaziv(String enPuniNaziv) {
		this.enPuniNaziv = enPuniNaziv;
	}

	/**
	 * Getter metoda
	 * @return	glavni grad države
	 */
	public String getGlavniGrad() {
		return glavniGrad;
	}

	/**
	 * Setter metoda
	 * @param glavniGrad	naziv glavnog grada
	 */
	public void setGlavniGrad(String glavniGrad) {
		this.glavniGrad = glavniGrad;
	}

	/**
	 * Getter metoda
	 * @return	ISo 2-slovna kratica države
	 */
	public String getKratica() {
		return kratica;
	}

	/**
	 * Setter metoda
	 * @param kratica	ISO 2-slovna kratica države
	 */
	public void setKratica(String kratica) {
		this.kratica = kratica;
	}

	/**
	 * Getter metoda
	 * @return	naziv datoteke himne države
	 */
	public String getHimna() {
		return himna;
	}

	/**
	 * Setter metoda
	 * @param himna	naziv datoteke himne
	 */
	public void setHimna(String himna) {
		this.himna = himna;
	}

	/**
	 * Getter metoda
	 * @return	naziv datoteke zastave države
	 */
	public String getZastava() {
		return zastava;
	}

	/**
	 * Setter metoda
	 * @param zastava	naziv datoteke zastave
	 */
	public void setZastava(String zastava) {
		this.zastava = zastava;
	}

	/**
	 * Getter metoda
	 * @return	naziv datoteke slike države
	 */
	public String getSlika() {
		return slika;
	}

	/**
	 * Setter metoda
	 * @param slika	naziv datoteke sa slikom države
	 */
	public void setSlika(String slika) {
		this.slika = slika;
	}

	/**
	 * Getter metoda
	 * @return	oznaka za pristup detaljima države
	 */
	public String getDetalji() {
		return detalji;
	}

	/**
	 * Setter metoda
	 * @param detalji	oznaka za pristup detaljima države
	 */
	public void setDetalji(String detalji) {
		this.detalji = detalji;
	}

	/**
	 * Getter metoda
	 * @return	id države
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter metoda
	 * @param id	id države
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Metoda za uspoređivanje dvije države. 
	 * Dvije države su jednake ako su im ISO kodovi jednaki.
	 * @param obj	objekt zapisa države koji se uspoređuje s "ovom" državom
	 * @return	true, ako su oba zapisa države jednaka, inača false
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Zapis)) {
			return false;
		}
		Zapis other = (Zapis) obj;
		// ISO kod 2 znamenkaste oznake države je jedinstven
		if (kratica == null) {
			if (other.kratica != null) {
				return false;
			}
		} else if (!kratica.equals(other.kratica)) {
			return false;
		}
		return true;
	}

	/**
	 * Metoda za uspoređivanje dvije države.
	 * Uspoređuju se po nazivu vodeći računa o hrvatskim slovima.
	 * @param obj	zapis države koji se uspoređuje s "ovim" zapisom države
	 * @return	0, pozitivan broj ili negativan broj ovisno o odnosu naziva država koje se uspoređuju.  
	 */
	@Override
	public int compareTo(Zapis obj) {
		Locale locale = new Locale("hr", "HR");
		Collator collator = Collator.getInstance(locale);
		return collator.compare(this.naziv,obj.naziv);
	}

	/**
	 * Prikaz svih podataka države kao niza znakova.
	 * @return	svi podaci zapisa države
	 */
	@Override
	public String toString() {
		return "Zapis [id = " + id + ", naziv=" + naziv + ", puniNaziv=" + puniNaziv + ", enNaziv=" + enNaziv + ", enPuniNaziv="
				+ enPuniNaziv + ", glavniGrad=" + glavniGrad + ", kratica=" + kratica + ", himna=" + himna
				+ ", zastava=" + zastava + ", slika=" + slika + " detalji=" + detalji + "]";
	}

	/**
	 * Metoda za uspoređivanje dvije države.
	 * Uspoređuju se po nazivu vodeći računa o hrvatskim slovima.
	 * @param o1	zapis prve države koja se uspoređuje 
	 * @param o2	zapis druge države koja se uspoređuje 
	 * @return	0, pozitivan broj ili negativan broj ovisno o odnosu naziva država koje se uspoređuju.  
	 */
	@Override
	public int compare(Zapis o1, Zapis o2) {
		Locale locale = new Locale("hr", "HR");
		Collator collator = Collator.getInstance(locale);
		return collator.compare(o1.getNaziv(),o2.getNaziv());
	}

}
