package hr.unidu.pr.projekt.pokazni.alati;
import java.net.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.*;
public class SkiniLink {

	public static void main(String[] args) throws Exception {
		SkiniLink s = new SkiniLink();
		ArrayList<String[]> drzave = s.napuniDrzave();
		String pocetak ="http://www.google.com/";
		//String slika = "croatia.png";
		int i = 0;
		for (String[] d : drzave){
			i++;
			System.out.println("Radi: " + i);
			s.dohvatiSliku(pocetak+s.trazi(d[1]),"slike/"+d[0]+".png", d[1]);
		}
	}
	public String trazi(String drzava)throws Exception
	{
		StringBuffer sb = new StringBuffer();
		//URL oracle = new URL("https://www.google.com/search?q="+drzava);
		URL oracle = new URL("https://www.google.com/search?q="+URLEncoder.encode(drzava));
		URLConnection yc = oracle.openConnection();
		yc.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36");
		BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
		PrintWriter b = new  PrintWriter(new FileWriter("stranica.html"));
		String inputLine;
		while ((inputLine = in.readLine()) != null)
			sb.append(inputLine);
		in.close();
		b.close();
		int pom = sb.indexOf("\"lu_map\" src=\"");
		int poc = pom+14;
		int kraj = sb.indexOf("\"",poc+1);
		String adr = sb.substring(poc,kraj);
		return adr;
	}
	private void dohvatiSliku(String imageUrl, String destinationFile, String drzava) {
	    URL url;
		try {
			url = new URL(imageUrl);
		
	    InputStream is = url.openStream();
	    OutputStream os = new FileOutputStream(destinationFile);

	    byte[] b = new byte[2048];
	    int length;

	    while ((length = is.read(b)) != -1) {
	        os.write(b, 0, length);
	    }
	    is.close();
	    os.close();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println("1. " +drzava);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("2. " +drzava);
		}
	}
	private ArrayList<String[]>napuniDrzave() throws FileNotFoundException{
		Scanner s = new Scanner(new File("drzave.csv"));
		String inputLine;
		ArrayList<String[]> drzave = new ArrayList<String[]>();
		//s.useDelimiter(";");
		boolean dosao = false;
		while (s.hasNextLine()){
			String line = s.nextLine();
		    String[] fs = line.split(";");
		    String[] d = new String[2];
		    d[0] = fs[2];
		    d[1] = fs[3];
		    if (!fs[4].equals("1")){
		       	d[0] = fs[2];
			    d[1] = fs[4];
//			    dosao = true;
			    drzave.add(d);
		    }
//		    if (dosao)
//		    	drzave.add(d);
	    	
		}
		s.close();
		return drzave;
	}
}
