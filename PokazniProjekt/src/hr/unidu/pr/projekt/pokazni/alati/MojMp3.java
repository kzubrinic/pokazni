package hr.unidu.pr.projekt.pokazni.alati;


import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;

import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;

/**
 * Klasa za reprodukciju Mp3 formata
 */
public class MojMp3 extends SviracGlazbe{
	private Player mp3Player;
	private PlayerThread pt;
	
	/**
	 * Konstruktor klase
	 * @param pjesma	mp3 datoteka za reprodukciju
	 */
	public MojMp3(String pjesma) {
		super(pjesma);
	}

	/**
	 * Metoda koja započinje reprodukciju mp3 datoteke
	 * @throws	IOException
	 * @throws	JavaLayerException
	 */
	public void pokreniGlazbu(){
		try {
			BufferedInputStream bis = new BufferedInputStream(new URL(getNazivPjesme()).openStream());
			if (mp3Player == null){
				mp3Player = new Player(bis);
				pt = new PlayerThread();
				pt.start();
			}
			else {
				pt.interrupt();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 
		catch (JavaLayerException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Metoda koja prekida reprodukciju mp3 datoteke
	 */
	public void prekiniGlazbu(){
		if (!(mp3Player == null)){
			mp3Player.close();
			mp3Player = null;
			setNazivPjesme(null);
		}
	}

	/**
	 * Unutarnja klasa koja se koristi za reprodukciju u posebnoj dretvi
	 */
	class PlayerThread extends Thread {
		/**
		 * Započinjanje reprodukcije mp3 datoteke
		 * @throws	Exception
		 */
		public void run() {
			try	{
				mp3Player.play();
			} catch( Exception e ){
				e.printStackTrace();
			}
		}
	}
}

