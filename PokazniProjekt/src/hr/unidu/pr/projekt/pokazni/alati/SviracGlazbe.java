package hr.unidu.pr.projekt.pokazni.alati;

/**
 * Apstraktna klasa koju trebaju naslijediti svirači za
 * reprodukciju datoteka u različitim formatima
 */
public abstract class SviracGlazbe {
	private String nazivPjesme;

	/**
	 * Konstruktor klase
	 * @param pjesma	naziv datoteke za reprodukciju
	 */
	public SviracGlazbe(String pjesma){
		nazivPjesme = pjesma; 
	}
	
	/**
	 * Getter metoda
	 * @return	vraća naziv datoteke koja se reproducira
	 */
	public String getNazivPjesme(){
		return nazivPjesme;
	}
	
	/**
	 * Setter metoda
	 * @param pjesma	naziv datoteke koja se reproducira
	 */
	public void setNazivPjesme(String pjesma){
		nazivPjesme = pjesma; 
	}
	
	/**
	 * Metoda koja započinje reprodukciju
	 */
	public abstract void pokreniGlazbu();

	/**
	 * Metoda koja prekida reprodukciju
	 */
	public abstract void prekiniGlazbu();
	
}
