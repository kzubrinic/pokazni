package hr.unidu.pr.projekt.pokazni;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class DrzaveListModelTest {
	private List<String> l;
	private void pripremiListu() {
		l = new ArrayList<>();
		l.add("prvi");
		l.add("drugi");
		l.add("druga");
		l.add("treći");
	}
	@Test
	public void testDrzaveListModel() {
		pripremiListu();
		DrzaveListModel<String> model = new DrzaveListModel<>(l);
		assertEquals(4, model.getSize());
	}

	@Test
	public void testGetElementAt_unutarRaspona() {
		pripremiListu();
		DrzaveListModel<String> model = new DrzaveListModel<>(l);
		assertEquals("prvi", model.getElementAt(0));
	}
	@Test
	public void testGetElementAt_vanRaspona() {
		pripremiListu();
		DrzaveListModel<String> model = new DrzaveListModel<>(l);
		assertEquals(null, model.getElementAt(4));
	}
	@Test
	public void testGetElementAt_pogresanIndeks() {
		pripremiListu();
		DrzaveListModel<String> model = new DrzaveListModel<>(l);
		assertEquals(null, model.getElementAt(-1));
	}

	@Test
	public void testGetSize() {
		pripremiListu();
		DrzaveListModel<String> model = new DrzaveListModel<>(l);
		assertEquals(4, model.getSize());
	}

	@Test
	public void testAddElement() {
		pripremiListu();
		DrzaveListModel<String> model = new DrzaveListModel<>(l);
		model.addElement("peti");
		assertEquals(5, model.getSize());
	}

	@Test
	public void testRemove() {
		pripremiListu();
		DrzaveListModel<String> model = new DrzaveListModel<>(l);
		model.remove("druga");
		assertEquals(3, model.getSize());
	}

	@Test
	public void testUpdateElement() {
		pripremiListu();
		DrzaveListModel<String> model = new DrzaveListModel<>(l);
		model.updateElement(1, "novi");
		assertEquals("novi", model.getElementAt(1));
	}

	@Test
	public void testFilter() {
		pripremiListu();
		DrzaveListModel<String> model = new DrzaveListModel<>(l);
		model.filter("d");
		assertEquals(2, model.getSize());
	}

}
