# INFORMACIJE O REPOZITORIJU POKAZNOG PROJEKTA #

Repozitorij sadrži pokazni projekt za kolegij "Projekt u 3. semestru" na preddiplomskom studiju Primijenjenog/poslovnog računarstva.

### Kako pristupiti sadržaju Projekta? ###

U dijelu [Downloads](https://bitbucket.org/kzubrinic/pokazni/downloads/ "Downloads") na Bitbucket poslužitelju možete pristupiti posljednjoj verziji dokumentacije, izvršne verzije programa i kompletnog projekta arhiviranog u jednoj datoteci.

Pored toga, uobičajenim naredbama [Git-a](https://tkrajina.github.io/uvod-u-git/git.pdf "Uvod u Git") može se dohvatiti izvorni kod programa.

### Kome je namijenjen ovaj repozitorij? ###

Repozitorij je namijenjen studentima i nastavnicima na kolegiju "Projekt u 3. semestru" kao ogledni primjerak.